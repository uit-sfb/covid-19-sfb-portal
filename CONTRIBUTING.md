## Issues

In case you encounter an [issue](https://gitlab.com/uit-sfb/covid-19-sfb-portal/-/issues) with the [live service](https://covid19.sfb.uit.no), please submit an issue and we will look at it as soon as possible.

## Gidelines for contributing

Note for internal developers: **any** commit to `master` will be **discarded**.

Please follow this procedure:
- If an issue doesn't yet exist, create one with as much details as possible
- Click on `create merge request`
- Checkout to the created branch and develop there
- When you are finished with your commit, go to the merge request's page and click on `Merge`.  
This should merge your working branch to `master` and delete it, as well as closing the issue.

## Notes about the CI/CD pipeline

The portal is opionally deployed in a staging environment after each commit at the address `covid19-s1.sfb.uit.no`.
This endpoint is accessible only from with UiT's sub-net.

Each release (with a name like `x.y.z`) creates a protected tag which triggers the updtae of the production deployment.
Note that protected tags cannot be delete (neither can the docker images be overridden),
which means that fixing an issue in a release should simply be done by creating a new release with an incremented version value (using [Sementic versioning scheme](https://semver.org/)).
