FROM caddy:2-builder AS builder

ARG PORTAL_VERSION
ARG COMMIT
ENV CI_COMMIT_REF_NAME=$PORTAL_VERSION
ENV CI_COMMIT_SHA=$COMMIT


RUN apk --no-cache add g++

RUN caddy-builder \
    github.com/caddyserver/nginx-adapter \
    github.com/vrongmeal/caddygit

FROM klakegg/hugo:ext-nodejs

ARG PORTAL_VERSION
ARG COMMIT
ENV CI_COMMIT_REF_NAME=$PORTAL_VERSION
ENV CI_COMMIT_SHA=$COMMIT


COPY --from=builder /usr/bin/caddy /usr/bin/caddy

WORKDIR /src

COPY ["src", "./"]
WORKDIR /src
RUN mkdir -p public_covid
RUN hugo --destination public_covid
RUN mv -f public_covid/_webapp-data/mar.xsd public_covid
WORKDIR /src/public_covid
COPY ["/.npmrc", "/package.json", "./"]
RUN npm --verbose install .

ENTRYPOINT ["caddy", "run", "--config", "/src/caddy.json"]

