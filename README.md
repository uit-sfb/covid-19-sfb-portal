# COVID-19 SFB PORTAL

Covid19 Portal is [SfB](https://en.uit.no/forskning/forskningsgrupper/gruppe?p_document_id=347053)'s service delivering highly curated Covid19 sequence contextual data.
The live service is available [here](https://covid19.sfb.uit.no) and is updated weekly with the newest genomic data.

The curation process executed by a semi-automatic workflow which enriches the [Covid19DB](https://gitlab.com/uit-sfb/sarscovid19db) database.

## Getting started

### Requirements

- Docker
- docker-compose (optional)


### Launching the portal

Run:
```
docker run --rm -p 1313:1313 registry.gitlab.com/uit-sfb/covid-19-sfb-portal/covidportal:<version>
```
, where `<version>` is the [latest version](https://gitlab.com/uit-sfb/covid-19-sfb-portal/-/releases).

The portal should now be accessible from your web browser at the address `localhost:1313`.
