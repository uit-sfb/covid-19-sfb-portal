---
date: 2016-12-22T14:09:00
title: Home
type: index
name: home
---

The SARS-CoV-2DB is a manually curated virus genome database for SARS-CoV-2 research compiled from publicly available resources. The portal also contains links to bioinformatics tools and data resources provided freely by ELIXIR Norway and ELIXIR members.

