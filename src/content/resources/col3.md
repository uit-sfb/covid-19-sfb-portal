---
date: "0001-01-01T00:00:00"
private: true
title: "Links to other ELIXIR resources"
type: "resourcesCol3"
---

__ELIXIR resources__ \
__[ELIXIR Norway](https://elixir.no/ELIXIRNO_COVID19)__ \
__[ELIXIR support to Covid-19 research](https://elixir-europe.org/covid-19-resources)__ \
__[Resources from ELIXIR members](https://elixir-europe.org/services/covid-19/what-we-are-doing)__
