---
date: "0001-01-01T00:00:00"
private: true
title: "SARS-CoV-2 database downloads"
type: "resourcesCol1"
---
For downloading the contextual or sequence databases, please follow these links: \
_[SARS-CoV-2 DB Sequence Data](https://s1.sfb.uit.no/public/mar/SarsCovid19DB/versioned_genomes/latest)_ \
_[SARS-CoV-2 DB BLAST database](https://s1.sfb.uit.no/public/mar/SarsCovid19DB/BLAST/latest)_ \
_[SARS-CoV-2 DB Contextual database](https://s1.sfb.uit.no/public/mar/SarsCovid19DB/contextual/latest)_
