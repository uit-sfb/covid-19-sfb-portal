---
date: "0001-01-01T00:00:00"
title: "Resources"
type: "index"
weight: 1.0
---

ELIXIR Norway and ELIXIR provide a number of resources that can be used by scientists working with SARS-CoV2 research.
