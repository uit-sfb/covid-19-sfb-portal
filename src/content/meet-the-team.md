---
date: "0001-01-01T00:00:00"
layout: "single"
title: "Documentation"
type: "meet-the-team"
---

<Br>
__The MMP Team__
<Br>
The SARS-CoV-2 Portal was developed as a part of the ELIXIR2 project, funded by the Research Council of Norway, and made online in April 2020. The Portal is maintained by The Center for Bioinformatics (SfB) at the UiT The Arctic University of Norway. SfB is hosting the UiT node of ELIXIR Norway. Below you will find a short description of who we build and implemented the SARS-CoV-2 database.
<Br>
<Br>

__The SARS-CoV-2 database__
<Br>
The content of SARS-CoV-2 database has been compiled from a number of publicly available resources such as sequence databases at [EMBL-EBI](https://www.ebi.ac.uk/), [NCBI](https://www.ncbi.nlm.nih.gov/), [DDBJ](https://www.ddbj.nig.ac.jp/index-e.html) and [China National Center for Bioinformation](https://bigd.big.ac.cn/ncov/?lang=en). In addition we have used public available data from [Johns Hopkins University Coronavirus Resource Centre](https://coronavirus.jhu.edu/) and litterature databases such as [Europe PMC](https://europepmc.org/) and [PubMed](https://www.ncbi.nlm.nih.gov/pubmed/) for curation.