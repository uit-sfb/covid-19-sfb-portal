---
date: "0001-01-01T00:00:00"
private: true
title: "Visualization"
type: "indexCol3"
---

The visualization page shows the geolocalization of genomes present in the SARS-CoV-2 databases in addition to the infection statistics.