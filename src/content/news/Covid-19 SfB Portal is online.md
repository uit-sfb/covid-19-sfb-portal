---
date: "2020-04-21T00:00:01+01:00"
title: "SARS-CoV-2 database launched"
---

The SARS-CoV-2 DB has now been launched. The database contains publicly available and curated sequence and metadata for complete SARS-CoV-2 genomes.
