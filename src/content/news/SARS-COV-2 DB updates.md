---
date: "2020-04-28T00:00:01+01:00"
title: "SARS-CoV-2 DB updates"
---

The SARS-CoV-2 DB will be updated weekly (every Monday) with newly published genomes. In today's update, there are approximately 1400 genomes, an increase from 865 genomes last week. All genomes have been manually curated to ensure consistency across the entries and to ease the filter and search function.