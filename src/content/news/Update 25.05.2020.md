---
date: "2020-05-25T00:00:01+01:00"
title: "SARS-CoV-2 DB 25.05.2020 update"
---

In today's update, we have compiled approximately 3920 genomes. All genomes have been manually curated to ensure consistency across the entries and to ease the filter and search function.