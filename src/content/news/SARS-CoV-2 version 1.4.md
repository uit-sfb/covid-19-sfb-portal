---
date: "2020-05-13T00:00:01+01:00"
title: "SARS-CoV-2 DB 1.4"
---

In this week's update, we have reached more than 2100 curated genomes. We will continue to 
updated the database each weekly with newly published genomes. 